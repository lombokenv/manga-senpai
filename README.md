# mangasenpai_apps

Aplikasi Seru Baca Manga Gratis "Manga Senpai" berbasis Android dan iOS

## Keterangan Project

Project ini dibangun menggunakan Flutter

Fitur-fitur yang ada pada Apps ini sbb:

- User dapat membaca manga 
- Guest dapat mendaftar menjadi member untuk mengakses kontent secara penuh
- User dapat menjadi creator manga
- ...

<!--For help getting started with Flutter, view our -->
<!--[online documentation](https://flutter.dev/docs), which offers tutorials, -->
<!--samples, guidance on mobile development, and a full API reference.-->
