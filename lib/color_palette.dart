import 'package:flutter/material.dart';

class ColorPalette{
  static const dotColor = Color(0xffe8e8e8);
  static const dotActiveColor = Color(0xffff5722);
  static const titleColor = Color(0xffff5722);
  static const wrapperContentColor = Color(0xfff5f5f5); 
  static const appBarColor = Color(0xffff5722);
  static const descriptionColor = Color(0xff707070);
  static const tabBarColor = Color(0xffe64a19);
  // static const titleColor = Color(0xfff4511e);
}