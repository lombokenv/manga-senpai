import 'package:flutter/material.dart';
import 'package:mangasenpai_apps/color_palette.dart';
import 'package:responsive_container/responsive_container.dart';

class DesciptionContent extends StatefulWidget{
  final data;
  DesciptionContent(this.data);
  @override
  _ContentList createState() => new _ContentList(this.data);
}

class _ContentList extends State<DesciptionContent>{
  final data;

  _ContentList(this.data);

  @override
  Widget build(BuildContext context) {
    Widget synopsis = new Container(
      color: Colors.white,
                   width: MediaQuery.of(context).size.width,
                   padding: EdgeInsets.symmetric(vertical: 3.0),
                   margin: EdgeInsets.only(bottom: 10),
                   child:new Column(
                   children: <Widget>[
                             new Padding(
                               padding: EdgeInsets.all(8.0),
                               child: new Text(data['synopsis'],
                                style: TextStyle(
                                  fontSize: 15.0,
                                  letterSpacing: 0.5,
                                  color: Colors.grey[700]
                                ),
                               ),
                             )
                   ],
               ),     
        );
    Widget sticky = new Container(
      color: ColorPalette.dotActiveColor,
                   width: MediaQuery.of(context).size.width,
                   padding: EdgeInsets.symmetric(vertical: 3.0),
                   child:new Row(
                     crossAxisAlignment: CrossAxisAlignment.center,
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: <Widget>[
                        new Padding(
                          padding: EdgeInsets.all(10),
                          child: new Text("Status : "+data["status"],
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.white
                            ),
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.all(10),
                          child: new Text("Rank : "+data["popularityRank"].toString(),
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.white
                            ),
                          ),
                        ),
                        new Padding(
                          padding: EdgeInsets.all(10),
                          child: new Text("Favorite : "+data["favoritesCount"].toString(),
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.white
                            ),
                          ),
                        ),
                     ],
                   )  
        );

        return new ResponsiveContainer(
          heightPercent: 100.0,
          widthPercent: 100.0,
          child: new Container(
              padding: EdgeInsets.all(0.0),
              color:ColorPalette.wrapperContentColor,
              child: new ListView(
                    padding: EdgeInsets.all(0),
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                            new Material(
                              borderRadius: BorderRadius.circular(4.0),
                              elevation:0.0,
                              child: new Container(
                                color: Colors.white,
                                 padding: EdgeInsets.all(8.0),
                                 child: new Column( 
                                   children: <Widget>[
                                     new Container(
                                       padding: EdgeInsets.only(top: 8.0,bottom: 15.0),
                                        decoration: new BoxDecoration(
                                          border: Border(
                                            bottom: BorderSide(
                                              color: Colors.grey[200]
                                            )
                                          ),
                                        ),
                                        child: new Align(
                                          alignment: Alignment.centerLeft,
                                          child: new Row(
                                            children: <Widget>[
                                                new Padding(
                                                  padding: EdgeInsets.only(left: 10),
                                                  child: new Text(
                                                  'Synopsis :',
                                                  style: TextStyle(
                                                    fontSize: 16.0,
                                                    color: Colors.grey[700],
                                                    fontWeight: FontWeight.w700
                                                  ),
                                                ),
                                                ),
                                                Spacer(),
                                                new Padding(
                                                  padding: EdgeInsets.only(right: 15),
                                                  child: new Icon(
                                                  Icons.star,
                                                  color: Colors.yellow[700],
                                                  size: 25,
                                                ),
                                                )
                                                
                                            ],
                                          )
                                        ),
                                     ),
                                     synopsis,
                                     sticky
                                   ],
                                 ),
                               ),
                            )
                         ],
                   
            ),
          )
        );
  }

}