import 'package:flutter/material.dart';
import 'package:mangasenpai_apps/color_palette.dart';
import '../model/action_profil.dart';
import '../screens/Aboutus.dart';
import '../screens/signin_metode.dart';

class DrawerContent extends StatefulWidget{

  @override
  _DrawerApp createState() => new _DrawerApp();
}

  

class _DrawerApp extends State<DrawerContent>{

  ScrollController scrollViewController;
  final List<ActionsButtonProfil> actionList = [
    ActionsButtonProfil(
      title: 'To Be Creator',
      icons: Icons.add,
      actions: new SigninMetode()
    ),
    ActionsButtonProfil(
      title: 'Invite Friends',
      icons: Icons.person_add,
      actions: null
    ),
    ActionsButtonProfil(
      title: 'Settings',
      icons: Icons.settings,
      actions: null
    ),
    ActionsButtonProfil(
      title: 'Feedback',
      icons: Icons.chat,
      actions: null
    ),
    ActionsButtonProfil(
      title: 'About Us',
      icons: Icons.info,
      actions: new AboutUs()
    ),


  ];

  @override
  void initState() {
    super.initState();
    scrollViewController = new ScrollController();
  }

  @override
  void dispose() {
    scrollViewController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      backgroundColor: ColorPalette.wrapperContentColor,

      body: new CustomScrollView(
        slivers: <Widget>[
           new SliverAppBar(
              elevation: 0,
              backgroundColor: ColorPalette.wrapperContentColor,
              pinned: true,
              floating: false,
              snap: false,
              automaticallyImplyLeading: true,
              iconTheme: IconThemeData(
                color: Colors.grey[700]
              ),
              centerTitle: true,
              title: new Text(
                'Profil',
                style: new TextStyle(
                  color: Colors.grey[700],
                  fontSize: 18.0
                ),
              ),
           ),
           new SliverPersistentHeader(
             delegate: CustomSliverDelegate(
               expandedHeight: 120,
               child: new Container(
                 height: MediaQuery.of(context).size.height,
                 color: ColorPalette.wrapperContentColor,
                 child: new Center(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Container(
                            width: 70.0,
                            height: 70.0,
                            decoration: new BoxDecoration(
                               color: Colors.white,
                               shape: BoxShape.circle,
                               image: new DecorationImage(
                                 fit: BoxFit.fill,
                                 image: new NetworkImage(
                                   "http://chittagongit.com/images/no-profile-picture-icon/no-profile-picture-icon-13.jpg"
                                 )
                               )
                            ),
                          ),
                          new Padding(
                              padding: EdgeInsets.all(10.0),
                              child :new Text("User",
                              textScaleFactor: 1,
                              style: TextStyle(
                                color: Colors.grey[700],
                                fontWeight: FontWeight.w500
                              ),
                              )
                          )
                        ],
                    )
                 ),
               )
             ),
           ),
           SliverList(
             delegate: SliverChildListDelegate([
               ...List.generate(1, (_){
                 return Row(
                   children: <Widget>[
                     buildItems(EdgeInsets.only(right: 1)
                     )
                   ],
                 );
               })
             ]),
           )

        ],
      ),
    );
  }

  Widget buildItems([EdgeInsetsGeometry margin]){
    return Expanded(
      child: Container(
          margin: margin,
          child: Column(
            children: _buildPage()
          ),
      ),
    );
  }

    List<Widget> _buildPage(){
    List<Widget> widgets = [];
    for(int i = 0; i < actionList.length; i++){
       ActionsButtonProfil abp = actionList[i];
       widgets.add(
          new Container(
              decoration: new BoxDecoration(
              border: new Border(
              bottom: BorderSide(
                        color: Colors.grey[200]
                      )
                    )
              ),
              child: new SizedBox(
                    width: double.infinity,
                    child: new RaisedButton(
                      elevation: 0,
                      child: new Align(
                        alignment: Alignment.bottomLeft,
                        child: new Row(
                          children: [
                              new Padding( 
                                padding: EdgeInsets.only(right: 15.0),
                                child: new Icon(abp.icons)
                              
                              ) //item icon
                              ,new Text(abp.title),
                              Expanded(
                                child: new Text(""),
                              ),
                              new Icon(Icons.keyboard_arrow_right,
                                color: Colors.grey[400],
                              ) //item title
                          ],
                        ),
                      ),
                      onPressed:(){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)
                            => abp.actions
                          )
                        );
                      },
                      color: Colors.white,
                      padding: EdgeInsets.only(top: 22,bottom: 22,left: 10,right: 10),
                      splashColor: Colors.grey,

                    ),
                ),
            )
       );
    }
    return widgets;
  }

}
class CustomSliverDelegate extends SliverPersistentHeaderDelegate{
  final double expandedHeight;
  final Widget child;

  CustomSliverDelegate(
    {
      @required this.expandedHeight,
      @required this.child,
    }
  );

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    // TODO: implement build
    return child;
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => expandedHeight;

  @override
  // TODO: implement minExtent
  double get minExtent => expandedHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    // TODO: implement shouldRebuild
    return true;
  }
}