import 'package:flutter/material.dart';
import 'package:mangasenpai_apps/color_palette.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:responsive_container/responsive_container.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class HomeContent extends StatefulWidget{

  @override
  _ContentList createState() => new _ContentList();
}
class _ContentList extends State<HomeContent>{
  List data;
  List dataGenresAnime;
  List dataPopularsAnime;

  final String url = "https://kitsu.io/api/edge/categories/7/anime";
  final String url1 = "https://kitsu.io/api/edge/categories/9/anime";
  final String url2 = "https://kitsu.io/api/edge/trending/anime";
 

  Future<String> getRankingAnime() async{
    var response = await http.get(Uri.encodeFull(url),
    headers: {"Accept": "application/vnd.api+json"}); 
    if(this.mounted){
      setState((){
            var extractdata = json.decode(response.body);
            data = extractdata['data']; 
            
        });
    }
  }
  Future<String> getGenresAnime() async{
    var response = await http.get(Uri.encodeFull(url1),
    headers: {"Accept": "application/vnd.api+json"}); 
    if(this.mounted){
      setState((){
            var extractdata = json.decode(response.body);
            dataGenresAnime = extractdata['data']; 
            
        });
    }
  }
  Future<String> getPopularAnime() async{
    var response = await http.get(Uri.encodeFull(url2),
    headers: {"Accept": "application/vnd.api+json"}); 
    if(this.mounted){
      setState((){
            var extractdata = json.decode(response.body);
            dataPopularsAnime = extractdata['data']; 
            
        });
    }
  }

  @override
  void initState() {
    super.initState();
    this.getRankingAnime();
    this.getGenresAnime();
    this.getPopularAnime();
  }  


  @override
  Widget build(BuildContext context) {

    Widget carouselPostNew = new Material( 
        elevation: 4.0,
        child : new SizedBox(
            height: 196.0,
            width: MediaQuery.of(context).size.width,
            child: new Carousel(
              boxFit: BoxFit.cover,
              images: [
                    new ExactAssetImage("assets/images/1.jpg"),
                    new ExactAssetImage("assets/images/2.jpg"),
                    new ExactAssetImage("assets/images/3.png"),
                    new ExactAssetImage("assets/images/4.jpg"),
                    new ExactAssetImage("assets/images/5.jpg"),
                    new ExactAssetImage("assets/images/6.jpeg"),
                    new ExactAssetImage("assets/images/7.png"),
                    new ExactAssetImage("assets/images/8.jpg"),
                    new ExactAssetImage("assets/images/9.jpg"),
                    new ExactAssetImage("assets/images/10.jpg"),
                    new ExactAssetImage("assets/images/11.jpg"),
              ],
                animationDuration: const Duration(milliseconds: 800),
                animationCurve: Curves.fastOutSlowIn,
                dotSize: 6.0,
                dotSpacing: 15.0,
                dotColor: ColorPalette.dotActiveColor,
                indicatorBgPadding: 10.0,
                dotBgColor: ColorPalette.wrapperContentColor.withOpacity(0.5),
                borderRadius: false,
            ),
        )
    );

    Widget coverMangaRecomendedItems = new Container(
      width: MediaQuery.of(context).size.width,
      child: new RefreshIndicator(
          onRefresh: ()async{
              this.getRankingAnime();
          },
          child: new AnimatedCrossFade(
            duration: new Duration(milliseconds: 1000),
            firstChild: new Center(
              child: new CircularProgressIndicator(),
            ),
            secondChild: new ListView.builder(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.all(0),
              itemCount: data == null ? 0 : data.length,
              itemBuilder: (BuildContext context, iterasi){
                return new Wrap(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      width: MediaQuery.of(context).size.width/3,
                      child: new Column(
                        children: <Widget>[
                          new Image.network(
                              data[iterasi]["attributes"]["posterImage"]["small"],
                              height: 123,fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                          ),
                          new Center(
                            child:new Padding(
                                padding: EdgeInsets.only(bottom: 3.0,top:3.0),
                                child: new Text(
                                  data[iterasi]["attributes"]["canonicalTitle"],
                                  style: TextStyle(fontWeight: FontWeight.bold,
                                    fontFamily: 'Roboto'
                                  ),
                                  textAlign: TextAlign.center,
                                  ), 
                              ),
                          ),
                          new Center(
                            child:new Padding(
                                padding: EdgeInsets.only(bottom: 3.0,top:3.0),
                                child: new Text(data[iterasi]["attributes"]["synopsis"].toString(),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                          ),
                        ],
                      )
                      
                    ),
                  ],
                );
              },
            ),
            crossFadeState: data !=null
            ? CrossFadeState.showSecond
            : CrossFadeState.showFirst,

          ),
      ),
    );
    
    Widget coverMangaPopularItems = new Container(
      width: MediaQuery.of(context).size.width,
      child: new RefreshIndicator(
          onRefresh: ()async{
              this.getRankingAnime();
          },
          child: new AnimatedCrossFade(
            duration: new Duration(milliseconds: 1000),
            firstChild: new Center(
              child: new CircularProgressIndicator(),
            ),
            secondChild: new ListView.builder(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.all(0),
              itemCount: data == null ? 0 : data.length,
              itemBuilder: (BuildContext context, iterasi){
                return new Wrap(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      width: MediaQuery.of(context).size.width/3,
                      child: new Column(
                        children: <Widget>[
                          new Image.network(
                              dataPopularsAnime[iterasi]["attributes"]["posterImage"]["small"],
                              height: 123,fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                          ),
                          new Center(
                            child:new Padding(
                                padding: EdgeInsets.only(bottom: 3.0,top:3.0),
                                child: new Text(
                                  dataPopularsAnime[iterasi]["attributes"]["canonicalTitle"],
                                  style: TextStyle(fontWeight: FontWeight.bold,
                                    fontFamily: 'Roboto'
                                  ),
                                  textAlign: TextAlign.center,
                                  ), 
                              ),
                          ),
                          new Center(
                            child:new Padding(
                                padding: EdgeInsets.only(bottom: 3.0,top:3.0),
                                child: new Text(dataPopularsAnime[iterasi]["attributes"]["synopsis"].toString(),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                          ),
                        ],
                      )
                      
                    ),
                  ],
                );
              },
            ), 
            crossFadeState: dataGenresAnime !=null
            ? CrossFadeState.showSecond
            : CrossFadeState.showFirst,

          ),
      ),
    ); 

    Widget coverMangaGenresItems = new Container(
      width: MediaQuery.of(context).size.width,
      child: new RefreshIndicator(
          onRefresh: ()async{
              this.getRankingAnime();
          },
          child: new AnimatedCrossFade(
            duration: new Duration(milliseconds: 1000),
            firstChild: new Center(
              child: new CircularProgressIndicator(),
            ),
            secondChild: new ListView.builder(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.all(0),
              itemCount: data == null ? 0 : data.length,
              itemBuilder: (BuildContext context, iterasi){
                return new Wrap(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      width: MediaQuery.of(context).size.width/3,
                      child: new Column(
                        children: <Widget>[
                          new Image.network(
                              dataGenresAnime[iterasi]["attributes"]["posterImage"]["small"],
                              height: 123,fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                          ),
                          new Center(
                            child:new Padding(
                                padding: EdgeInsets.only(bottom: 3.0,top:3.0),
                                child: new Text(
                                  dataGenresAnime[iterasi]["attributes"]["canonicalTitle"],
                                  style: TextStyle(fontWeight: FontWeight.bold,
                                    fontFamily: 'Roboto'
                                  ),
                                  textAlign: TextAlign.center,
                                  ), 
                              ),
                          ),
                          new Center(
                            child:new Padding(
                                padding: EdgeInsets.only(bottom: 3.0,top:3.0),
                                child: new Text(dataGenresAnime[iterasi]["attributes"]["synopsis"],
                                overflow: TextOverflow.ellipsis,),
                              ),
                          ),
                        ],
                      )
                      
                    ),
                  ],
                );
              },
            ),
            crossFadeState: dataGenresAnime !=null
            ? CrossFadeState.showSecond
            : CrossFadeState.showFirst,

          ),
      ),
    );    

    Widget recomendedList = new Card(
    margin: EdgeInsets.symmetric(vertical: 8.0),
    child: new Container(
        margin: EdgeInsets.symmetric(vertical:7.0,horizontal: 10.0),
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: new Wrap(
          children: <Widget>[
              new Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(top: 3,bottom: 10),
                child:new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Text('Recommend',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w600,letterSpacing: 0.1,color: ColorPalette.titleColor),)
                      ),
                      new InkWell(
                        onTap:(){
                            print('more recomended');
                        },
                        child :new Text('See All',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w500,color: Colors.grey),)
                      )
                    ],
                ) ,
              ),
              new Container(
                height: 180.0,
                width: MediaQuery.of(context).size.width,
                child: coverMangaRecomendedItems
              ),
             
          ],
        )

    )
    );

    
    Widget popularList = new Card(
    margin: EdgeInsets.symmetric(vertical: 8.0),
    child: new Container(
        margin: EdgeInsets.symmetric(vertical:7.0,horizontal: 10.0),
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: new Wrap(
          children: <Widget>[
              new Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(top: 3,bottom: 10),
                child:new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Text('Popular',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w600,letterSpacing: 0.1,color: ColorPalette.titleColor),)
                      ),
                      new InkWell(
                        onTap:(){
                            print('more recomended');
                        },
                        child :new Text('See All',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w500,color: Colors.grey),)
                      )
                    ],
                ) ,
              ),
              new Container(
                height: 180.0,
                width: MediaQuery.of(context).size.width,
                child: coverMangaPopularItems
              ),
          ],
        )

    )
    );

    Widget genresList = new Card(
    margin: EdgeInsets.symmetric(vertical: 8.0),
    child: new Container(
        margin: EdgeInsets.symmetric(vertical:7.0,horizontal: 10.0),
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: new Wrap(
          children: <Widget>[
              new Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(top: 3,bottom: 10),
                child:new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Text('Genres',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w600,letterSpacing: 0.1,color: ColorPalette.titleColor),)
                      ),
                      new InkWell(
                        onTap:(){
                            print('more recomended');
                        },
                        child :new Text('See All',style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w500,color: Colors.grey),)
                      )
                    ],
                ) ,
              ),
              new Container(
                width: MediaQuery.of(context).size.width,
                height: 180.0,
                child: coverMangaGenresItems
                   
              ),
          ],
        )

    )
    );

    //Ads Container
    Widget adsContent = new Material(
          elevation: 4.0,
          borderRadius: BorderRadius.circular(10.0),
          child : new Container(
            width: MediaQuery.of(context).size.width,
            height:150.0,
            child: new Image.asset('assets/images/10.jpg',fit: BoxFit.cover,),
          )
    );

    // return Scaffold(
        return new ResponsiveContainer(
              widthPercent: 100.0,
              heightPercent: 100.0,
              child: new ListView(
                padding: EdgeInsets.all(0),
                children: <Widget>[  
                      new Container(
                        padding: EdgeInsets.all(8.0),
                        color: ColorPalette.wrapperContentColor, 
                        child: new Column(
                            children: <Widget>[
                              carouselPostNew,
                              recomendedList,
                              popularList, 
                              genresList,
                              adsContent,
                            ],
                          ), 
                    )
                ],
              ),
    );
  }


}