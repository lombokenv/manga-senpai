import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:mangasenpai_apps/model/intro.dart';
import 'package:mangasenpai_apps/screens/homepage_view.dart';
import 'package:mangasenpai_apps/color_palette.dart';


class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  final List<Intro> introList = [
    Intro(
      image: "assets/images/sdd.jpg",
      title: "Introduce", 
      description: "Hello Senpai! Apakah kamu siap menjadi Manga Senpai berikutnya?"+
                    " yuk gabung bersama kami dan update terus gambar mangamu di sini."+
                    " Tunjukkan bahwa manga mu adalah manga terbaik di Manga Senpai",
    ),
    Intro(
      image: "assets/images/ccc.png",
      title: "What is Manga Senpai?",
      description: "Manga Senpai adalah aplikasi jejaring manga dimana kamu bisa"+
                    " tunjukin manga terbaikmu dengan orang-orang pencinta manga"+
                    " di seluruh dunia",
    ),
    Intro(
      image: "assets/images/sdsdd.jpg",
      title: "Are you ready to be\n the next Manga Senpai?",
      description: "Jika kamu telah siap, yuk! gabung dengan kita. Karena di Manga Senpai"
                    " kamu bisa mendapatkan rating kamu di sini dan bertemu dengan Manga Senpai terbaik"+
                    " di seluruh dunia",
    ),
  ]; 


  @override
  Widget build(BuildContext context) {
      Widget titleSection = Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height/6,
        padding: EdgeInsets.only(bottom: 0),
        child: new Align(
          alignment: Alignment.bottomCenter,
          child: new Text(
                'MANGA SENPAI',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1.0,
                  color: ColorPalette.dotActiveColor,
                  fontSize:19,
                )
          ),
        )
      );

      Widget navButton = Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height/5.8,       
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
             RaisedButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
                },
                child: Text("Get started",
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: "NotoSans"
                        
                    )
                ),
                padding:EdgeInsets.only(left: 25,right: 25,top: 13,bottom: 13),
                textColor: Colors.white,
                color: ColorPalette.dotActiveColor,
            ),
          ],
        ),
      );

      Widget onboardScreen = Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height/1.6,
          child: Swiper.children(
                index: 0,
                autoplay: true,
                loop: false,
                pagination: SwiperPagination(
                builder: DotSwiperPaginationBuilder(
                    color: ColorPalette.dotColor,
                    activeColor: ColorPalette.dotActiveColor,
                    size: 10.0,
                    activeSize: 10.0,
                 ),
                ),
                control: SwiperControl(
                  iconNext: null,
                  iconPrevious: null
                ),

                children: _buildPage(context)
                ),
      );
    return Scaffold(
        backgroundColor: Colors.white,
        body: new Wrap(
            direction: Axis.vertical,
            children: <Widget>[
                titleSection,
                onboardScreen,
                navButton
            ],
        ),
    );
  }

  List<Widget> _buildPage(BuildContext context) {

    List<Widget> widgets = [];
    for(int i=0; i<introList.length; i++) {
      Intro intro = introList[i];
      widgets.add(
        Container(
          child: ListView(
            children: <Widget>[
              Image.asset(
                intro.image,
                height: MediaQuery.of(context).size.height/3.4,
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 10,
                ),
              ),
              Center(
                child: Text(
                  intro.title,
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 19.0,
                    letterSpacing: 0.5,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto'
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 13.0,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.height/20.0,
                ),
                child: Text(
                  intro.description,
                  style: TextStyle(
                    color: ColorPalette.descriptionColor,
                    letterSpacing: 0.5,
                    fontSize: 15.0,
                    fontFamily: 'Roboto'
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      );
    }
    return widgets;
  }


}