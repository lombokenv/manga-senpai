import 'package:flutter/material.dart';
import 'package:mangasenpai_apps/color_palette.dart';
import 'package:responsive_container/responsive_container.dart';

class FavContent extends StatefulWidget{
  @override
  _ContentList createState() => new _ContentList();
}

class _ContentList extends State<FavContent>{

  @override
  Widget build(BuildContext context) {
    Widget cardsContent = new Container(
               child: new Container(
                        decoration: new BoxDecoration(
                                border: Border(
                                bottom: BorderSide(
                                color: Colors.grey[200]
                               )
                          ),
                        ),
                        width: MediaQuery.of(context).size.width,
                        height: 100.0,
                        padding: EdgeInsets.symmetric(vertical: 3.0),
                        child:new Row(
                        children: <Widget>[
                             new Image.asset("assets/images/4.jpg",width: 110,height: MediaQuery.of(context).size.height,fit: BoxFit.cover,),
                        ],
               ),     
           ),
        );

        return new ResponsiveContainer(
          heightPercent: 100.0,
          widthPercent: 100.0,
          child: new Container(
              padding: EdgeInsets.all(8.0),
              color:ColorPalette.wrapperContentColor,
              child: new ListView(
                    padding: EdgeInsets.all(0),
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                            new Material(
                              borderRadius: BorderRadius.circular(4.0),
                              elevation:3.0,
                              child: new Container(
                                color: Colors.white,
                                 padding: EdgeInsets.all(8.0),
                                 child: new Column( 
                                   children: <Widget>[
                                     new Container(
                                       padding: EdgeInsets.only(top: 4.0,bottom: 10.0),
                                        decoration: new BoxDecoration(
                                          border: Border(
                                            bottom: BorderSide(
                                              color: Colors.grey[200]
                                            )
                                          ),
                                        ),
                                        child: new Align(
                                          alignment: Alignment.centerLeft,
                                          child: new Text(
                                            'Your Favorite Manga',
                                            style: TextStyle(
                                              fontSize: 18.0,
                                              color: Colors.grey[700]
                                            ),
                                          ),
                                        ),
                                     ),
                                     cardsContent,
                                     cardsContent,
                                     cardsContent,
                                     cardsContent,
                                     cardsContent,
                                     cardsContent,
                                     cardsContent,
                                   ],
                                 ),
                               ),
                            )
                         ],
                   
            ),
          )
        );
  }

}