import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class SigninMetode extends StatefulWidget{

  @override
  _SigninMetodeContent createState() => new _SigninMetodeContent();
}

class _SigninMetodeContent extends State<SigninMetode>{
  
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = new GoogleSignIn();

  Future<FirebaseUser> _signIn() async{
    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    GoogleSignInAuthentication gSA = await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: gSA.accessToken,
      idToken: gSA.idToken
    );
    FirebaseUser user = await _auth.signInWithCredential(
      credential
    );
    print("Username : ${user.displayName}");
    return user;

  }

  void _signOut(){
    googleSignIn.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/xa.jpeg"),
            fit: BoxFit.cover,
          ),
        ),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: new Center(
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                  new RaisedButton(
                      color: Colors.amber,
                      onPressed: (){print('gmail');},
                      padding: EdgeInsets.only(left: 53.0,right: 53.0,top: 17.0,bottom: 17.0),
                      child: new Text(
                          'Log with Senpai account',
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 15.0,
                            color: Colors.white
                            ),
                          )
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: new Container(
                          height: 50.0,
                          child: SignInButton(
                          Buttons.Facebook,
                          onPressed: () {
                            _signOut();
                          },
                        ),
                      ),
                    ),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: new Container(
                      height: 50.0,
                      child: SignInButton(
                        Buttons.Google,
                        onPressed: () {
                          _signIn()
                          .then((FirebaseUser user) => print(user))
                          .catchError((e) => print(e));

                          Navigator.pop(context);
                        
                        }
                      ),
                    ),
                  ),
                  
                ],
            ),
          )
      ),
    );
  }

}