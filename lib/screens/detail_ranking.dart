import 'package:flutter/material.dart';
import 'package:mangasenpai_apps/color_palette.dart';
import 'package:responsive_container/responsive_container.dart';
import 'package:collection/equality.dart';
import '../screens/description_detailinfo.dart';
class DetailRank extends StatefulWidget{

  final data;
  
  DetailRank(this.data);

  @override
  DetailRankingContent  createState() => new DetailRankingContent(this.data);

}


class DetailRankingContent extends State<DetailRank> with SingleTickerProviderStateMixin{
  static GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  TabController tabController;
  ScrollController scrollViewController;
  final data;


  DetailRankingContent(this.data);


  @override
  void initState() {
    super.initState();
    tabController = new TabController(vsync: this, length: 3);
    scrollViewController = new ScrollController();
  }
  @override
  void dispose() {
    tabController.dispose();
    scrollViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

        Widget tabBarView = new TabBarView(
          controller: tabController,
          children: <Widget>[
            DesciptionContent(data),
            new Text('data2'),
            new Text('data2')

          ],
        );

        return Scaffold(
          body: new NestedScrollView(
              controller: scrollViewController,
              headerSliverBuilder: (BuildContext context,bool boxIsScrolled){
                return <Widget>[                 
                  SliverAppBar(
                    elevation: 0,
                    backgroundColor: ColorPalette.dotActiveColor,
                     expandedHeight: 157.0,
                     titleSpacing: 0,
                     flexibleSpace: new FlexibleSpaceBar(
                       centerTitle: true,
                       title: new Wrap(
                          direction: Axis.vertical,
                          children: <Widget>[
                            new Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              new Center(
                                child:new Align(
                                    alignment: Alignment.bottomCenter,
                                    child: new Text(data["canonicalTitle"].toString(),
                                            style: new TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.white,
                                            shadows: [
                                                Shadow(
                                                  blurRadius: 0.4,
                                                  color: Colors.black,
                                                  offset: Offset(1.0, 1.0),
                                                ),
                                              ],
                                            ),
                                          ),
                                      ),
                                ),
                                new Center(
                                  child:new Align(
                                    alignment: Alignment.bottomCenter,
                                    child: new Text(data["titles"]["ja_jp"].toString(),
                                            style: new TextStyle(
                                            fontSize: 13.0,
                                            color: Colors.white,
                                            shadows: [
                                                Shadow(
                                                  blurRadius: 0.4,
                                                  color: Colors.black,
                                                  offset: Offset(1.0, 1.0),
                                                ),
                                              ],
                                            ),
                                          ),
                                      ),
                                )
                              ]
                            )
                          ]
                       ),
                       background: new Image.network(
                         "${data["coverImage"]["original"].toString()} != null ? 'https://i.imgur.com/BoN9kdC.png'  : 'https://i.imgur.com/BoN9kdC.png'",
                         fit: BoxFit.cover,
                       ),
                     ),
                     automaticallyImplyLeading: true,
                     pinned: true,
                     floating: false,
                     forceElevated: boxIsScrolled,
                  ),

                SliverPersistentHeader(
                  delegate: _SliverAppBarDelegate(

                    TabBar(
                      indicator: UnderlineTabIndicator(
                          insets: EdgeInsets.symmetric(horizontal: 24.0)
                      ),
                      labelColor: ColorPalette.dotActiveColor,
                      unselectedLabelColor: Colors.grey,
                      tabs: [
                        Tab(icon: Icon(Icons.info), text: "Description"),
                        Tab(icon: Icon(Icons.play_arrow), text: "Episodes"),
                        Tab(icon: Icon(Icons.comment), text: "Comments"),
                      ],
                      controller: tabController,
                    ),
                  ),
                  pinned: true,
                  ),
                ];
              },
              body: tabBarView,
          ),

        );
}
}


class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new Container(
      child: new Material(
        elevation: 2,
        color: Colors.white,
        child: _tabBar,
      ),
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}