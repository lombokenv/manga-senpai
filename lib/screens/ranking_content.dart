import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:responsive_container/responsive_container.dart';
import '../screens/detail_ranking.dart';

class RankingContent extends StatefulWidget{

    @override
      MyRankingContent createState() => new MyRankingContent();
} 


class MyRankingContent extends State<RankingContent> {
  List data;
  final String url = "https://kitsu.io/api/edge/trending/manga";

  Future<String> getRankingAnime() async{
    var response = await http.get(Uri.encodeFull(url),
    headers: {"Accept": "application/vnd.api+json"});
    // print(response.body);    
    if(this.mounted){
      setState((){
            var extractdata = json.decode(response.body);
            data = extractdata['data']; 
        });
    }
  }

  @override
  void initState() {
    super.initState();
    this.getRankingAnime();
    // getAllAnime();
  }
  @override
  Widget build(BuildContext context) {

    return new Container(
      padding: EdgeInsets.all(8.0),
      child: new RefreshIndicator(
        onRefresh: ()async{
          this.getRankingAnime();
        }, 
        child: new AnimatedCrossFade(
          duration: new Duration(milliseconds: 2000),
          firstChild: new Center(
            child: new CircularProgressIndicator(),
          ),
          secondChild: new ListView.builder(
                padding: EdgeInsets.all(0),
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, iterasi){
                  return new Wrap(
                    children: <Widget>[
                        new Material(
                            elevation: 3.0,
                            child:  new Container(
                                      padding: EdgeInsets.all(0),
                                      decoration: new BoxDecoration(
                                      border: new Border(
                                      bottom: BorderSide(
                                                color: Colors.grey[200]
                                              )
                                            )
                                      ),
                                      child: new SizedBox(
                                            width: double.infinity,
                                            child: new RaisedButton(
  
                                      elevation: 0,
                                      child: new Align(
                                        alignment: Alignment.bottomLeft,
                                        child: new Wrap(
                                          direction: Axis.horizontal,
                                          children: [
                                              new ResponsiveContainer(
                                                padding:EdgeInsets.all(0),
                                                widthPercent: 20.0,
                                                heightPercent: 15.0,
                                                child: new Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  height: MediaQuery.of(context).size.height,
                                                      child: new Image.network(
                                                      data[iterasi]["attributes"]["posterImage"]["small"],fit: BoxFit.cover,
                                                  ),
                                                )
                                              ),
                                              new ResponsiveContainer(
                                                widthPercent: 70.0,
                                                heightPercent: 15.0,
                                                child: new ListTile(
                                                        title: new Text(data[iterasi]["attributes"]["canonicalTitle"],style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w600,color: Colors.grey[600]),),
                                                        subtitle: new Wrap(
                                                          direction: Axis.vertical,
                                                          children: <Widget>[
                                                            new Padding(
                                                              padding:EdgeInsets.only(top:3,bottom: 3),
                                                              child: new Text("Rank: "+data[iterasi]["attributes"]["ratingRank"].toString(),style: TextStyle(fontSize: 15.5)),
                                                            
                                                            ),
                                                            new Padding(
                                                              padding:EdgeInsets.only(top:3,bottom: 3),
                                                              child: new Text("Status: "+data[iterasi]['attributes']["status"],style: TextStyle(fontSize: 15.5)),
                                                            ),
                                                            new Padding(
                                                              padding:EdgeInsets.only(top:3,bottom: 3),
                                                              child: new Text("Type: "+data[iterasi]['attributes']["mangaType"],style: TextStyle(fontSize: 15.5)),
                                                            )
                                                          ],
                                                        ),
                                                  ),
                                              )
                                          ],
                                        ),
                                      ),
                                      onPressed:(){
                                        // print(data[iterasi]["mal_id"].toString());
                                        Navigator.push(
                                          context, 
                                          new MaterialPageRoute(
                                             builder: (BuildContext context)
                                             => new DetailRank(data[iterasi]["attributes"])
                                          )

                                        );
                                      },
                                      color: Colors.white,
                                      padding: EdgeInsets.only(top: 10,bottom: 10,left: 4,right: 3),
                                      splashColor: Colors.grey,

                                    ),
                                ),
                            ),
                          ),
                    ],
                    
                  );
                },
              ),
          crossFadeState: data != null
            ? CrossFadeState.showSecond
            : CrossFadeState.showFirst
          ,
        ),
      )
    );
  }
}
