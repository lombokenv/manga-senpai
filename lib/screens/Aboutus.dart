import 'package:flutter/material.dart';
import 'package:mangasenpai_apps/color_palette.dart';

class AboutUs extends StatefulWidget{

      @override
      _AboutUsContent createState() => new _AboutUsContent();
}

class _AboutUsContent extends State<AboutUs> with SingleTickerProviderStateMixin{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: 
        AppBar(
                elevation: 0,
                backgroundColor: ColorPalette.wrapperContentColor,
                automaticallyImplyLeading: true,
                iconTheme: IconThemeData(
                  color: Colors.grey[700]
                ),
                centerTitle: true,
                title: new Text(
                  'About Us',
                  style: new TextStyle(
                    color: Colors.grey[700],
                    fontSize: 18.0
                  ),
                ),
            ),
      body: new Center(
          child: new Container(
            width: MediaQuery.of(context).size.width,
            child: new Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: new Text('Manga Senpai adalah sebuah aplikasi yang dibangun oleh'+
                    " Mahasiswa Teknik Informatika Universitas Mataram yakni Rhomy Idris Sardi"+
                    " dan Satria Malaca. Manga Senpai di dirikan sejak tahun 2019 dengan tujuan awal"+
                    " memenuhi salah satu tugas matakuliah Pemrograman Bergerak.",
                    style: TextStyle(
                      fontFamily: "Roboto",
                      fontSize: 15,
                      
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
                  child: new Text("Manga Senpai dibangun untuk menggali potensi kreator manga"+
                    " di Indonesia. Jadi bukan hanya sebuah wadah yang dapat dimanfaatkan sebagai"+
                    " penempatan karya manga, Manga Senpai menyediakan fitur menarik lain seperti"+
                    " membaca manga trending yang ada di Indonesia dan Jepang.",
                    style: TextStyle(
                      fontFamily: "Roboto",
                      fontSize: 15,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
                  child: new Text("Version : v0.1 MangaSenpai (Beta)",
                              style: TextStyle(
                                fontFamily: "Roboto",
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.left,
                            ),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
                  child: new Text("Support : Android & iOS",
                              style: TextStyle(
                                fontFamily: "Roboto",
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.left,
                            ),
                ),
                new Container(
                  padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
                  child: new Text("Founder : Rhomy Idris Sardi, Satria Malaca",
                              style: TextStyle(
                                fontFamily: "Roboto",
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.left,
                            ),
                ),
              ],
            ),
          ),
      ) 
    );
  }

}