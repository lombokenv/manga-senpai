import 'package:flutter/material.dart';
import 'package:mangasenpai_apps/color_palette.dart';
import './home_content.dart' as home_content;
import './favorite_content.dart' as fav_content;
import './daily_content.dart' as daily_content;
import './ranking_content.dart' as rank_content;
import 'package:mangasenpai_apps/screens/drawer_content.dart';

class HomePage extends StatefulWidget{

      @override
      _HomeState createState() => new _HomeState();
}

class _HomeState extends State<HomePage> with SingleTickerProviderStateMixin{

  static GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  TabController tabController;
  ScrollController scrollViewController;

  @override
  void initState() {
    super.initState();
    tabController = new TabController(vsync: this, length: 4);
    scrollViewController = new ScrollController();
  }

  @override
  void dispose() {
    tabController.dispose();
    scrollViewController.dispose();
    super.dispose();
  }
      @override
      Widget build(BuildContext context) {
        Widget searchBar = new Container(
                height: 47.0,
                margin: EdgeInsets.only(top: 8.0),
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                color: ColorPalette.appBarColor,
                child: new Material(
                      elevation: 2,
                      borderRadius: new BorderRadius.circular(2.5),
                      child: new Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: <Widget>[
                             new Padding(
                                padding:EdgeInsets.all(5.0),
                                child: new IconButton(
                                  icon: new Icon(
                                    Icons.menu,
                                    color: ColorPalette.appBarColor,
                                    size:24.0,
                                  ),
                                  onPressed: (){
                                    // print("your menu action here");
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context)
                                          => new DrawerContent()
                                      )
                                    );
                                      //disini openDrawer
                                  },
                                  )
                             ),
                             Expanded(
                               child: new Container(
                                  padding: EdgeInsets.only(left: 10.0),
                                  child : new TextField(
                                    decoration: InputDecoration.collapsed(
                                      hintText: "MANGA SENPAI",
                                      hintStyle: TextStyle(
                                        fontSize: 19.0,
                                        fontWeight: FontWeight.bold,
                                        color: ColorPalette.dotActiveColor.withOpacity(0.7),
                                        fontFamily: 'NotoSans',
                                        letterSpacing: 0.9
                                      ),
                                    ),
                                    obscureText: false,
                                    onSubmitted: (String place){
                                       print(place);
                                    },
                                  ),
                               )
                             ),
                             new Padding(
                                padding:EdgeInsets.all(5.0),
                                child: IconButton(
                                    icon: new Icon(
                                      Icons.notifications,
                                      color: ColorPalette.appBarColor,
                                      size: 24.0,
                                    ),
                                    onPressed: (){
                                      print("your notify action here");
                                    })
                             )
                           ],
                      ),
                    ),
                );

        Widget tabBarView = new TabBarView(
          controller: tabController,
          children: <Widget>[
            new home_content.HomeContent(),
            new rank_content.RankingContent(),            
            new daily_content.DailyContent(),
            new fav_content.FavContent(),

          ],
        );

        return Scaffold(
          body: new NestedScrollView(
              controller: scrollViewController,
              headerSliverBuilder: (BuildContext context,bool boxIsScrolled){
                return <Widget>[
                  SliverAppBar(
                     title: searchBar,
                     titleSpacing: 0,
                     backgroundColor: ColorPalette.appBarColor,
                     automaticallyImplyLeading: false,
                     pinned: true,
                     floating: true,
                     forceElevated: boxIsScrolled,
                     bottom: TabBar(
                       indicator: UnderlineTabIndicator(
                          insets: EdgeInsets.symmetric(horizontal: 4.0)
                       ),
                       tabs: <Widget>[
                          Tab(
                            child: new Text('HOME',style: TextStyle(
                              fontFamily: 'NotoSans',
                              fontWeight: FontWeight.w400,
                              letterSpacing: 0.2
                            ),),
                          ),
                          Tab(
                            child: new Text('TOP',style: TextStyle(
                              fontFamily: 'NotoSans',
                              fontWeight: FontWeight.w400,
                              letterSpacing: 0.2
                            ),),
                          ),
                          Tab(
                            child: new Text('DAILY',style: TextStyle(
                              fontFamily: 'NotoSans',
                              fontWeight: FontWeight.w400,
                              letterSpacing: 0.2
                            ),),
                          ),
                          Tab(
                            child: new Text('FAVORITE',style: TextStyle(
                              fontFamily: 'NotoSans',
                              fontWeight: FontWeight.w400,
                              letterSpacing: 0.2
                            ),),
                          ),
                       ],
                       controller: tabController,
                     ),
                  )
                ];
              },
              body: tabBarView,
          ),

        );
      }
}